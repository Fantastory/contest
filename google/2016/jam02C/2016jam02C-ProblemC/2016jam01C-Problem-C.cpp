#include "stdafx.h"

#include <direct.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <stdint.h>
#include <assert.h>
#include <unordered_set>
#include <deque>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
#include <boost/numeric/ublas/matrix.hpp>

#include <helper/Io.h>
#include <helper/CheckIo.h>

using namespace std;
using namespace boost::numeric;
using namespace helpers;

//string prepath = "..\\2016-C\\";
string prepath = "";

string testin = "C-large-practice";
//string testin = "C-small-practice";
//string testin = "test";
//string testin = "C-small-attempt0";
//string testin = "C-large";

int work();
int test();
int check();

int main()
{
	test();
	work();
	check();
	doexit();
	return 0;
}

int test() {
	printf("tests done\n");
	return 0;
}

int outfitId(uint8_t j, uint8_t p, uint8_t s) {
	return s % 10 + (p % 10) * 10 + (j % 10) * 100;
}

int solve(Io &io) {
	unsigned j, p, s, k;

	string line = io.readLine();
	sscanf(line.c_str(), "%u %u %u %u\n", &j, &p, &s, &k);

	//throw away those shirts

	//get j with lowest usage
	//get p with lowest usage //or lowest usage with given j?
	//get s with lowest usage //or lowest usage with given j and p?


	
	unsigned mks = k;
	if (mks > s) mks = s;

	int maxOutfits = j * p * mks;
	io.outf("%d\n", maxOutfits);

	unsigned sp = 0;
	for (unsigned jp = 1; jp <= j; ++jp) {
		for (unsigned pp = 1; pp <= p; ++pp) {
			sp = jp + pp;
			for (unsigned sk = 1; sk <= mks; ++sk) {
				sp = jp + pp + sk;
				if (sp > s) {
					sp %= s;
					if (sp < 1) sp = s;
				}
				io.outf("%d %d %d\n", jp, pp, sp);
			}
		}
	}
	

	return 0;
}

int work() {
	Io io(testin, prepath);

	int problemc;
	string line = io.readLine();
	int ret = sscanf(line.c_str(), "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}
	printf("problemc %d\n", problemc);

	for (int n = 0; n < problemc; ++n) {
		io.outf("Case #%d: ", n + 1);

		solve(io);
	}

	return 0;
}

unsigned id(unsigned j, unsigned p, unsigned s) {
	return s + p * 100 + j * 10000;
}

int checkOne(CheckIo &io, int t) 
{
	stringstream strm;

	unsigned mj, mp, ms, mk;

	string line;
	readLine(io.m_in, line);
	int ret = sscanf(line.c_str(), "%d %d %d %d\n", &mj, &mp, &ms, &mk);
	if (ret != 4) doexit("scanf input\n");

	unsigned outfitc;
	readLine(io.m_out, line);
	ret = sscanf(line.c_str(), "%d\n", &outfitc);
	if (ret != 1) doexit("scanf outfitc\n");

	uint8_t jps[1000] = { 0 };
	uint8_t jp[1000] = { 0 };
	uint8_t js[1000] = { 0 };
	uint8_t ps[1000] = { 0 };

	for (unsigned i = 0; i < outfitc; ++i)
	{
		unsigned j, p, s;
		readLine(io.m_out, line);
		ret = sscanf(line.c_str(), "%d %d %d\n", &j, &p, &s);
		if (ret != 3) doexit("scanf j p s\n");

		if (j < 1 || p < 1 || s < 1) doexit((stringstream&) (strm << "j=="<<j<<" < 1 || p=="<<p<<" < 1 || s=="<<s<<" < 1"));
		if (j > mj || p > mp || s > ms) doexit((stringstream&) (strm <<"j=="<<j<<" > mj=="<<mj<<" || p=="<<p<<" > mp=="<<mp<<" || s=="<<s<<" > ms=="<<ms));

		if (++jps[outfitId(j, p, s)] > 1) doexit("j p s already used\n");
		if ( ++jp[outfitId(j, p, 0)] > mk) doexit("j p used > k\n");
		if ( ++js[outfitId(j, 0, s)] > mk) doexit("j s used > k\n");
		if ( ++ps[outfitId(0, p, s)] > mk) doexit("p s used > k\n");
	}

	return 0;
}

int check() {
	CheckIo io(testin, prepath);
	printf("\n--- check ---\n");

	int problemc;
	string line;
	readLine(io.m_in, line);
	int ret = sscanf(line.c_str(), "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}

	char buf[1024];

	for (int t = 1; t <= problemc; ++t) {
		printf("problem %d\n", t);

		int size = sprintf(buf, "Case #%d: ", t);

		string readBuf(size, ' ');

		fread(&readBuf[0], 1, readBuf.size(), io.m_out);
		if (readBuf != buf) {
			printf("case line != %s\n", buf);
			printf("case line == %s\n", readBuf.c_str());
			doexit();
		}

		
		checkOne(io, t);
	}
	return 0;
}

