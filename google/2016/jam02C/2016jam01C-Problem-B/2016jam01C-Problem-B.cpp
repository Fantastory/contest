#include "stdafx.h"

#include <direct.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <stdint.h>
#include <assert.h>
#include <unordered_set>
#include <deque>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/max_cardinality_matching.hpp>
#include <boost/numeric/ublas/matrix.hpp>

#include <helpers/helper/Io.h>
#include <helpers/helper/CheckIo.h>

using namespace std;
using namespace boost::numeric;
using namespace helpers;


//string prepath = "..\\2016-C\\";
string prepath = "";

string testin = "B-large-practice";
//string testin = "B-small-practice";
//string testin = "test";
//string testin = "B-small-attempt0";
//string testin = "B-large";

int work();
int test();
int check();


int main()
{
	test();
	work();
	check();
	doexit();
	return 0;
}

vector<uint8_t> twoPowers(uint64_t m) {
	vector<uint8_t> powers;
	while (m > 0) {
		powers.push_back(m % 2);
		m /= 2;
	}
	return powers;
}


int test() {
	printf("tests done\n");
	return 0;
}

int solve(Io &io) {
	uint64_t paths;
	unsigned hills;

	string line = io.readLine();
	sscanf(line.c_str(), "%d %I64u\n", &hills, &paths);

	vector<uint8_t> powers = twoPowers(paths - 1);

	//we will not use matrix[0][_] and matrix[_][0]
	ublas::matrix<int> matrix(hills + 1, hills + 1, 0);
	
	//full graph for all nodes greater than 1
	// 2->3, 2->4, 2->5, ... 3->3, 3->5 ... ...
	for (unsigned from = 2; from <= hills; ++from) {
		for (unsigned to = from + 1; to <= hills; ++to) {
			matrix(from,to) = 1;
		}
	}

	//path for the substracted one node
	matrix(1,hills) = 1;

	//paths according to binary mode

	//2^0 is path to (hills-1) ===> (hills-1)-0
	//2^1 is path to (hills-2) ===> (hills-1)-1
	//2^p is path to (hills-p) ===> (hills-1)-p
	for (unsigned p = 0; p < powers.size(); ++p) {

		if (powers[p]) {

			int to = (hills - 1) - p;
			if (to <= 1) {
				io.outf(" IMPOSSIBLE\n");
				return 0;
			}

			matrix(1, to) = 1;
		}
	}

	io.outf(" POSSIBLE\n");

	for (unsigned from = 1; from <= hills; ++from) {
		for (unsigned to = 1; to <= hills; ++to) {
			io.outf("%d", matrix(from,to));
		}
		io.outf("\n");
	}
	
	return 0;
}

int work() {
	Io io(testin, prepath);

	int problemc;
	string line = io.readLine();
	int ret = sscanf(line.c_str(), "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}
	printf("problemc %d\n", problemc);

	for (int n = 0; n < problemc; ++n) {
		io.outf("Case #%d:", n + 1);

		solve(io);
	}

	return 0;
}

int checkOne(CheckIo &io, int t) {
	string line;
	readLine(io.m_in, line);

	printf("case %d\n", t);

	int hills;
	uint64_t paths;
	int ret = sscanf(line.c_str(), "%d %I64u\n", &hills, &paths);
	if (ret != 2) doexit("first line scanf != 2");

	printf("hills %u paths %I64u\n", hills, paths);

	readLine(io.m_out, line);
	char buf[128];
	int casec;
	ret = sscanf(line.c_str(), "Case #%d: %s", &casec, &buf);
	if (ret != 2) doexit("casse scanf != 2");
	if (casec != t) doexit("cassec != t");

	uint64_t maxpaths = powll(2, hills - 2);
	if (paths > maxpaths) {
		if (buf != string("IMPOSSIBLE")) {
			printf("was >%s<, should be impossible 2^(%d-2) = %I64u < %I64u\n",
				buf, 
				hills, maxpaths, paths				
				);
			doexit();
		}
		return 0;
	}

	if (buf != string("POSSIBLE")) {
		printf("was >%s<, sohuld be POSSIBLE 2^(%d-2) = %I64u >= %I64u\n",
			buf,
			hills, maxpaths, paths
		);
		doexit();
	}
	
	ublas::matrix<int> matrix(hills + 1, hills + 1, 0);
	for (unsigned x = 1; x < matrix.size1(); ++x) {
		readLine(io.m_out, line);
		if (line.size() != hills+1) {
			printf("to less hills %d shouldbe %u\n", ((int) line.size()) -1, hills);
			doexit();
		}

		for (int y = 1; y <= hills; ++y) {
			char c = line[y-1] - '0';
			
			if (c < 0 || c > 1) {
				doexit("c < 0 || c > 1");
			}

			matrix(x, y) = c;
		}
	}

	//no backword paths from x >= y
	for (int x = 1; x <= hills; ++x) {
		for (int y = 1; y <= x; ++y) {
			if (matrix(x, y)) {
				printf("backwadrd path %d -> %d\n", x, y);
				doexit();
			}
		}
	}

	//full paths x(2,n) -> y>x
	for (int x = 2; x <= hills; ++x) {
		for (int y = x+1; y <= hills; ++y) {
			if (!matrix(x, y)) {
				printf("missing path %d -> %d\n", x, y);
				doexit();
			}
		}
	}

	uint64_t actualPaths = matrix(1, hills);
	uint64_t pow2 = 1;
	for (int y = hills-1; y > 0; --y) {
		if (matrix(1, y)) {
			actualPaths += pow2;
		}
		pow2 *= 2;
	}
	
	if (actualPaths != paths) {
		printf("%I64u == actualPaths != paths == %I64u", actualPaths, paths);
		doexit();
	}

	return 0;
}

int check() {
	CheckIo io(testin, prepath);

	printf("\n--- check ---\n");

	int problemc;
	string line;
	readLine(io.m_in, line);
	int ret = sscanf(line.c_str(), "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}

	for (int t = 1; t <= problemc; ++t) {
		checkOne(io, t);
	}	

	return 0;
}

