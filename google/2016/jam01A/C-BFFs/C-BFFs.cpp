#include "stdafx.h"

#include <direct.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <stdint.h>
#include <assert.h>
#include <unordered_set>
#include <deque>

using namespace std;

//string prepath = "..\\2016-C\\";
string prepath = "";
string testin = "test";
//string testin = "A-small-attempt0";
//string testin = "C-small-attempt1";
//string testin = "A-large";


int work();
int test();
int doexit() {
	printf("exiting\n");
	char c;
	scanf("%c", &c);
	exit(0);
}

int main()
{
	test();
	work();
	return 0;
}

int test() {

	printf("tests done\n");
	return 0;
}

int solve(FILE *fin, FILE *fout) {
	int n;

	fscanf(fin, "%d\n", &n);
	printf(">%d< \n", n);
	
	std::vector<int> bffs;
	bffs.resize(n, 0);
	for (unsigned i = 1; i < bffs.size(); ++i) {
		fscanf(fin, "%d", &bffs[i]);
		bffs[i]--;
	}

	for (unsigned i = 1; i < bffs.size(); ++i) {
		printf("%d ", bffs[i]);
	}
	printf("\n");

	/*
	vector<int> leader;
	leader.resize(bffs.size(), 0);
	for (int i = 0; i < leader.size(); ++i) {
		leader[i] = i;
	}
	*/

	//size of linked kids starting with kid [x]
	vector<int> linkedSizes;
	linkedSizes.resize(bffs.size(), 0);

	//find all cycles
	unordered_set<int> notcycled;
	for (unsigned i = 0; i < bffs.size(); ++i) {
		notcycled.insert(i);
	}

	while (notcycled.size() > 0) {
		
		unordered_set<int> currentCycle;

		const int startingKid = *notcycled.begin();
		int currentKid = startingKid;
		bool foundCycle = false;
		while (true) {
			unordered_set<int>::_Pairib pairib = currentCycle.insert(currentKid);
			if (pairib.second == false) {
				foundCycle = true;
				break;
			}

			int erased = notcycled.erase(currentKid);
			if (!erased) {
				//cycle for this kid was already found no need to check again
				break;
			}

			currentKid = bffs[currentKid];
		}
		
		if (!foundCycle) {
			continue;
		}

		//count size of current cycle
		int currentCycleSize = 0;
		const int startingKid = currentKid;
		while (true) {
			currentKid = bffs[currentKid];
			currentCycleSize++;
			if (currentKid == startingKid) {
				break;
			}
		}

		if (largestCycleSize < currentCycleSize)
			largestCycleSize = currentCycleSize;
	}

	fprintf(fout, " %d\n", largestCycleSize);
	printf(" %d\n", largestCycleSize);

	return 0;
}

int work() {
	string path = prepath + testin + ".in";
	FILE *fin = fopen(path.c_str(), "r");
	if (fin == NULL) {
		printf("can not open in %s\n", path.c_str());
		char *cwd = _getcwd(NULL, 0);
		printf("cwd %s\n", cwd);
		doexit();
	}

	path = prepath + testin + "_out.in";
	FILE *fout = fopen(path.c_str(), "w");
	if (fout == NULL) {
		printf("can not open out %s\n", path.c_str());
		doexit();
	}
	fflush(fout);

	int problemc;
	int ret = fscanf(fin, "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}
	printf("problemc %d\n", problemc);

	for (int n = 0; n < problemc; ++n) {
		fprintf(fout, "Case #%d:", n + 1);
		printf("Case #%d:", n + 1);

		solve(fin, fout);
	}

	fclose(fout);

	doexit();
	return 0;
}

