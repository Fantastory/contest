#include "stdafx.h"

#include <direct.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <stdint.h>
#include <assert.h>
#include <unordered_set>
#include <deque>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>

using namespace std;

//string prepath = "..\\2016-C\\";
string prepath = "";
string testin = "test";
//string testin = "A-small-attempt0";
//string testin = "C-small-attempt1";
//string testin = "A-large";


int work();
int test();
int doexit() {
	printf("exiting\n");
	char c;
	scanf("%c", &c);
	exit(0);
}

int main()
{
	test();
	work();
	return 0;
}

int test() {

	printf("tests done\n");
	return 0;
}

struct Kid {
	int kid;
	int bff;
	int time;
};

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS> BffGraph;

template < typename TimeMap >
class dfs_time_visitor : public boost::default_dfs_visitor {
	typedef typename property_traits < TimeMap >::value_type T;
public:
	dfs_time_visitor(TimeMap dmap, TimeMap fmap, T & t)
		: m_dtimemap(dmap), m_ftimemap(fmap), m_time(t) {
	}
	template < typename Vertex, typename Graph >
	void discover_vertex(Vertex u, const Graph & g) const
	{
		put(m_dtimemap, u, m_time++);
	}
	template < typename Vertex, typename Graph >
	void finish_vertex(Vertex u, const Graph & g) const
	{
		put(m_ftimemap, u, m_time++);
	}
	TimeMap m_dtimemap;
	TimeMap m_ftimemap;
	T & m_time;
};

int solve(FILE *fin, FILE *fout) {
	int n;

	fscanf(fin, "%d\n", &n);
	printf(">%d< \n", n);

	std::vector<Kid> kids;
	kids.resize(n, 0);
	for (unsigned i = 1; i < kids.size(); ++i) {
		int bff;
		fscanf(fin, "%d", &bff);
		bff--;

		kids[i].bff = bff;
		kids[i].kid = i;
	}
	
	vector<std::pair<int, int>> bffEdges;
	for (unsigned i = 1; i < kids.size(); ++i) {
		bffEdges.push_back(make_pair(i, kids[i].bff));
	}

	BffGraph bffGraph(bffEdges.begin(), bffEdges.end(), kids.size());

	boost::depth_first_search( )

	bffGraph.


	return 0;
}

int work() {
	string path = prepath + testin + ".in";
	FILE *fin = fopen(path.c_str(), "r");
	if (fin == NULL) {
		printf("can not open in %s\n", path.c_str());
		char *cwd = _getcwd(NULL, 0);
		printf("cwd %s\n", cwd);
		doexit();
	}

	path = prepath + testin + "_out.in";
	FILE *fout = fopen(path.c_str(), "w");
	if (fout == NULL) {
		printf("can not open out %s\n", path.c_str());
		doexit();
	}
	fflush(fout);

	int problemc;
	int ret = fscanf(fin, "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}
	printf("problemc %d\n", problemc);

	for (int n = 0; n < problemc; ++n) {
		fprintf(fout, "Case #%d:", n + 1);
		printf("Case #%d:", n + 1);

		solve(fin, fout);
	}

	fclose(fout);

	doexit();
	return 0;
}

