#include "stdafx.h"

#include <direct.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <stdint.h>
#include <assert.h>
#include <unordered_map>
#include <deque>

using namespace std;

//string prepath = "..\\2016-C\\";
string prepath = "";
string testin = "test";
//string testin = "B-small-attempt0";
//string testin = "C-small-attempt1";
//string testin = "B-large";


int work();
int test();
int doexit() {
	printf("exiting\n");
	char c;
	scanf("%c", &c);
	exit(0);
}

int main()
{
	test();
	work();
	return 0;
}

int test() {
	printf("tests done\n");
	return 0;
}

class TrainList : public vector<int> {
public:
	//bool operator <

};

class ListList : public vector<TrainList> {
public:
	void printme() const {
		const ListList &lists = *this;

		printf("ListList:\n");
		for (unsigned l = 0; l < lists.size(); ++l) {
			const TrainList &nextList = lists[l];

			for (unsigned i = 0; i < nextList.size(); ++i) {
				printf("%d ", nextList[i]);
			}
			printf("\n");
		}
	}
};

// do not inherit different logic types....
class Grid : public ListList {
	//check if this list fits at Grid[x][.]
	bool fitsInX(const TrainList &trainList, int x, int ysize) const {
		const Grid &grid = *this;

		for (unsigned i = 0; i < ysize; ++i) {
			const int &match = grid[x][i];
			if (match != 0 && trainList[i] != match) {
				return false;
			}
		}

		return true;
	}
};

int solve(FILE *fin, FILE *fout) {
	int n;
	fscanf(fin, "%d\n", &n);
	printf("%d\n", n);

	ListList lists;
	lists.resize(2 * n - 1);

	for (unsigned l = 0; l < lists.size(); ++l) {
		TrainList &nextList = lists[l];
		nextList.resize(n);

		for (unsigned i = 0; i < nextList.size(); ++i) {
			fscanf(fin, "%d", &nextList[i]);
		}
		fscanf(fin, "\n");
	}

	std::sort(lists.begin(), lists.end());

	lists.printme();
	
	// create grid
	ListList grid;
	grid.resize(n);
	for (unsigned l = 0; l < grid.size(); ++l) {
		TrainList &nextList = grid[l];
		nextList.resize(n, 0);
	}

	grid.printme();

	//grid[x][y]
	//x next line we can add to
	int x = 0;
	int y = 0;

	int missingx = -1;
	int missingy = -1;
	
	for (unsigned l = 0; l < lists.size(); ++l) {
		const TrainList &nextList = lists[l];
		/*
		//try to fit this list somewhere
		if (x <= y) {
			//check if it fits in next x
			bool fitx = grid.fi
				

		} else {

		}

		*/
	}

	//fprintf(fout, " %s\n", output.c_str());
	//printf(" %s\n", output.c_str());

	return 0;
}

int work() {
	string path = prepath + testin + ".in";
	FILE *fin = fopen(path.c_str(), "r");
	if (fin == NULL) {
		printf("can not open in %s\n", path.c_str());
		char *cwd = _getcwd(NULL, 0);
		printf("cwd %s\n", cwd);
		doexit();
	}

	path = prepath + testin + "_out.in";
	FILE *fout = fopen(path.c_str(), "w");
	if (fout == NULL) {
		printf("can not open out %s\n", path.c_str());
		doexit();
	}
	fflush(fout);

	int problemc;
	int ret = fscanf(fin, "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}
	printf("problemc %d\n", problemc);

	for (int n = 0; n < problemc; ++n) {
		fprintf(fout, "Case #%d:", n + 1);
		printf("Case #%d:", n + 1);

		solve(fin, fout);
	}

	fclose(fout);

	doexit();
	return 0;
}

