#include "stdafx.h"

#include <direct.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <stdint.h>
#include <assert.h>
#include <unordered_set>
#include <deque>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/graph/max_cardinality_matching.hpp>

using namespace std;

//string prepath = "..\\2016-C\\";
string prepath = "";
//string testin = "test";
//string testin = "C-small-practice";
//string testin = "C-small-attempt0";
string testin = "C-large";

string readString(FILE *f, int limit);
int work();
int test();
int doexit() {
	printf("exiting\n");
	fflush(NULL);
	char c;
	scanf("%c", &c);
	exit(0);
}

int main()
{
	test();
	work();
	return 0;
}

typedef pair<int, int> Topic;

//using namespace boost;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS> Graph;

//Graph g(used_by, used_by + sizeof(used_by) / sizeof(Edge), N);

//typedef graph_traits<Graph>::vertex_descriptor Vertex;

int nextId(map<string, int> &words, const string &word) {
	auto pair_ib = words.insert(make_pair(word, -1));
	if (pair_ib.second == true) {
		pair_ib.first->second = words.size();
	}
	return pair_ib.first->second;
}

int test() {
	printf("tests done\n");
	return 0;
}

int solve(FILE *fin, FILE *fout) {
	int n;

	fscanf(fin, "%d\n", &n);
	printf(">%d< \n", n);

	map<string, int> words;
	vector<Topic> topics;
	topics.reserve(n);

	for (int i = 0; i < n; ++i) {
		string firstWord = readString(fin, 30) + 'A';
		string secondWord = readString(fin, 30) + 'B';

		int firstId = nextId(words, firstWord);
		int secondId = nextId(words, secondWord);

		topics.push_back(std::make_pair(firstId, secondId));
	}

	for each(auto topic in topics) {
		printf("%d - %d\n", topic.first, topic.second);

	}

	Graph g(topics.begin(), topics.end(), words.size());

	std::vector<boost::graph_traits<Graph>::vertex_descriptor> mate(g.m_vertices.size());
	auto matching = &mate[0];

	boost::edmonds_maximum_cardinality_matching(g, matching);
	int matchingSize = boost::matching_size(g, matching);

	int fakec = n - matchingSize - (words.size() - 2 * matchingSize);
	fprintf(fout, " %d\n", fakec);
	printf(" %d\n", fakec);

	return 0;
}

int work() {
	string path = prepath + testin + ".in";
	FILE *fin = fopen(path.c_str(), "r");
	if (fin == NULL) {
		printf("can not open in %s\n", path.c_str());
		char *cwd = _getcwd(NULL, 0);
		printf("cwd %s\n", cwd);
		doexit();
	}

	path = prepath + testin + "_out.in";
	FILE *fout = fopen(path.c_str(), "w");
	if (fout == NULL) {
		printf("can not open out %s\n", path.c_str());
		doexit();
	}
	fflush(fout);

	int problemc;
	int ret = fscanf(fin, "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}
	printf("problemc %d\n", problemc);

	for (int n = 0; n < problemc; ++n) {
		fprintf(fout, "Case #%d:", n + 1);
		printf("Case #%d:", n + 1);

		solve(fin, fout);
	}

	fclose(fout);

	doexit();
	return 0;
}


string readString(FILE *f, int limit) {
	string buf;
	buf.resize(limit);

	fscanf(f, "%s", (char *)buf.data());

	size_t pos = buf.find('\0');
	buf.resize(pos);
	return buf;
}

