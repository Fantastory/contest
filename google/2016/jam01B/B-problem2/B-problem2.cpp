#include "stdafx.h"

#include <direct.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <stdint.h>
#include <assert.h>
#include <unordered_set>
#include <deque>

using namespace std;

//string prepath = "..\\2016-C\\";
string prepath = "";
string testin = "test";
//string testin = "B-small-attempt0";
//string testin = "B-large";

string readString(FILE *f, int limit);
int work();
int test();
int doexit() {
	printf("exiting\n");
	char c;
	scanf("%c", &c);
	exit(0);
}

int main()
{
	test();
	work();
	return 0;
}

int test() {

	printf("tests done\n");
	return 0;
}

struct MaxMin {
	uint64_t max;
	uint64_t min;
};

bool canBeEqal(const string &scoreStrC, const string &scoreStrJ) {
	for (int i = 0; i < scoreStrC.size(); ++i) {
		if (scoreStrC[i] == scoreStrJ[i]) {
			continue;
		}
		if (scoreStrC[i] == '?' ||
			scoreStrJ[i] == '?') {
			continue;
		}
		return false;
	}
	return true;
}

bool makeEqal(string &scoreStrC, string &scoreStrJ) {
	for (int i = 0; i < scoreStrC.size(); ++i) {
		if (scoreStrC[i] == '?') {
			if (scoreStrJ[i] == '?') {
				scoreStrJ[i] = '0';
			}
			scoreStrC[i] = scoreStrJ[i];
		}
		else 
		{
			if (scoreStrJ[i] == '?') {
				scoreStrJ[i] = scoreStrC[i];
			}
			else if (scoreStrJ[i] != scoreStrC[i])
			{
				return false;
			}
		}
	}
	return true;
}

int solve(FILE *fin, FILE *fout) {

	const int ccc = 0;
	const int jjj = 1;

	string scoreStrs[2];

	scoreStrs[ccc] = readString(fin, 205);
	scoreStrs[jjj] = readString(fin, 205);

	printf("\n%s\n%s\n", scoreStrs[ccc].c_str(), scoreStrs[jjj].c_str());

	if (canBeEqal(scoreStrs[ccc], scoreStrs[jjj])) {
		makeEqal(scoreStrs[ccc], scoreStrs[jjj]);
		fprintf(fout, " %s %s", scoreStrs[ccc].c_str(), scoreStrs[jjj].c_str());
	}
	
	vector<MaxMin> minMaxs[2];
	minMaxs[ccc].resize(scoreStrs[ccc].size());
	minMaxs[jjj].resize(scoreStrs[ccc].size());

	for (int ttt = ccc; ttt <= jjj; ++ttt) {
		uint64_t pow10 = 1;
		
		vector<MaxMin> &minMax = minMaxs[ttt];
		string &scoreStr = scoreStrs[ttt];

		for (int i = minMax.size() - 1; i >= 0; --i) {
			MaxMin &mm = minMax[i];

			const char &cchar = scoreStr[i];
			if (cchar == '?') {
				mm.min = 0;
				mm.max = 9;
			}
			else {
				mm.min = mm.max = ((uint64_t)(cchar - '0'));
			}
			mm.min *= pow10;
			mm.max *= pow10;
			
			unsigned prev = i + 1;
			if (prev < minMax.size()) {
				mm.min += minMax[prev].min;
				mm.max += minMax[prev].max;
			}
			pow10 *= 10;
		}
	}

	printf("%I64u\n%I64u\n%I64u\n%I64u\n", 
		minMaxs[ccc][0].min,
		minMaxs[ccc][0].max,
		minMaxs[jjj][0].min,
		minMaxs[jjj][0].max
	);


	//one of team must be winning
	int cWins = 0; //positive c team win, negative j win, 0 undecided
	for (int i = 0; i < scoreStrs[ccc].size(); i++) {
		if (cWins > 0) {
			if (scoreStrs[ccc][i] == '?') scoreStrs[ccc][i] = '0';
			if (scoreStrs[jjj][i] == '?') scoreStrs[jjj][i] = '9';
			continue;
		}
		if (cWins < 0) {
			if (scoreStrs[ccc][i] == '?') scoreStrs[ccc][i] = '9';
			if (scoreStrs[jjj][i] == '?') scoreStrs[jjj][i] = '0';
			continue;
		}

		if (scoreStrs[ccc][i] != '?' &&
			scoreStrs[jjj][i] != '?') 
		{
			if (scoreStrs[ccc][i] > scoreStrs[jjj][i]) {
				cWins = 1;
				continue;
			}
			else if (scoreStrs[ccc][i] < scoreStrs[jjj][i]) {
				cWins = -1;
				continue;
			}
			else {
				//they are still equal
				continue;
			}
		}
	}

	return 0;
}

int work() {
	string path = prepath + testin + ".in";
	FILE *fin = fopen(path.c_str(), "r");
	if (fin == NULL) {
		printf("can not open in %s\n", path.c_str());
		char *cwd = _getcwd(NULL, 0);
		printf("cwd %s\n", cwd);
		doexit();
	}

	path = prepath + testin + "_out.in";
	FILE *fout = fopen(path.c_str(), "w");
	if (fout == NULL) {
		printf("can not open out %s\n", path.c_str());
		doexit();
	}
	fflush(fout);

	int problemc;
	int ret = fscanf(fin, "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}
	printf("problemc %d\n", problemc);

	for (int n = 0; n < problemc; ++n) {
		fprintf(fout, "Case #%d:", n + 1);
		printf("Case #%d:", n + 1);

		solve(fin, fout);
	}

	fclose(fout);

	doexit();
	return 0;
}


string readString(FILE *f, int limit) {
	string buf;
	buf.resize(limit);
	
	fscanf(f, "%s", (char *)buf.data());
	
	size_t pos = buf.find('\0');
	buf.resize(pos);
	return buf;
}
