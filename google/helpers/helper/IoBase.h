#pragma once

#define _CRT_SECURE_NO_WARNINGS

#include <string>
#include <strstream>
#include <sstream>

#define __attribute__(attr)

namespace helpers {

	template <typename T> T* dropConst(T const * t) {
		return (T*)t;
	}

	void readLine(FILE *f, std::string &buffer);

	int doexit(const std::string &error = "");
	int doexit(std::stringstream &error);

	uint64_t powll(uint64_t base, uint64_t exp);

	class IoBase
	{
	public:
		std::string m_inName;
		std::string m_outName;

		FILE *m_in = NULL;
		FILE *m_out = NULL;

		IoBase(const std::string &input, const std::string &prepath = "");

		~IoBase();
	};

}

