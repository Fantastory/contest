#include <stdafx.h>
#include <cstdarg>

#include "Io.h"

using namespace std;

namespace helpers {





	Io::Io(const std::string &input, const std::string &prepath)
		: IoBase(input, prepath)
	{
		m_out = fopen(m_outName.c_str(), "w");
		if (m_out == NULL) {
			printf("can not open out %s\n", m_outName.c_str());
			doexit();
		}

		m_debName = prepath + input + "_deb.in";
		m_deb = fopen(m_debName.c_str(), "w");
		if (m_deb == NULL) {
			printf("can not open deb %s\n", m_debName.c_str());
			doexit();
		}
	}

	Io::~Io()
	{
		fclose(m_deb);
	}

	//TODO test
	string Io::readLine() {
		static string buf(20, ' ');
		
		helpers::readLine(m_in, buf);

		debf("%s", buf.c_str());
		return buf;
	}

	int Io::outf(const char *__format, ...) {
		va_list vl;
		va_start(vl, __format);
		{
			va_list vl_use;
			va_copy(vl_use, vl);
			vfprintf(m_out, __format, vl_use);
			va_end(vl_use);
		}

		{
			va_list vl_use;
			va_copy(vl_use, vl);
			vfprintf(m_deb, __format, vl_use);
			va_end(vl_use);
		}

		int ret = vfprintf(stdout, __format, vl);
		va_end(vl);
		fflush(NULL);
		return ret;
	}


	int Io::debf(const char *__format, ...) {
		va_list vl;
		va_start(vl, __format);

		{
			va_list vl_use;
			va_copy(vl_use, vl);
			vfprintf(m_deb, __format, vl_use);
			va_end(vl_use);
		}

		int ret = vfprintf(stdout, __format, vl);
		va_end(vl);
		fflush(NULL);
		return ret;
	}

}
