#include "stdafx.h"
#include "CheckIo.h"

namespace helpers {

	CheckIo::CheckIo(const std::string &input, const std::string &prepath)
		: IoBase(input, prepath)
	{
		m_out = fopen(m_outName.c_str(), "r");
		if (m_out == NULL) {
			printf("can not open out %s\n", m_outName.c_str());
			doexit();
		}

	}

	CheckIo::~CheckIo()
	{
	}

}
