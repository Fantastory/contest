#include <stdafx.h>
#include "IoBase.h"
#include <strstream>

using namespace std;

namespace helpers {

	int doexit(stringstream &err) {
		string error = err.str();
		int ret = doexit(error);

		stringstream strm;

		return ret;
	}

	int doexit(const string &error) {
		printf("exiting %s\n", error.c_str());
		fflush(NULL);
		char c;
		scanf("%c", &c);
		exit(0);
	}

	void readLine(FILE *f, string &buf) {
		buf.resize(buf.capacity());

		if (buf.size() < 1) buf.resize(10);

		unsigned bytes = 0;
		while (true) {
			int sizeLeft = buf.size() - bytes;
			char * data = fgets(dropConst(buf.data()) + bytes, sizeLeft, f);
			if (!data) {
				break;
			}
			int readSize = strlen(data);
			bytes += readSize;
			if (data[readSize - 1] == '\n') {
				break;
			}

			buf.resize(buf.size() * 2 + 1);
		}

		buf.resize(bytes);
	}

	uint64_t powll(uint64_t base, uint64_t exp)
	{
		uint64_t result = 1ULL;
		while (exp)
		{
			if (exp & 1)
			{
				result *= base;
			}
			exp >>= 1;
			base *= base;
		}
		return result;
	}

	IoBase::IoBase(const std::string &input, const std::string &prepath)
	{
		m_inName = prepath + input + ".in";
		m_in = fopen(m_inName.c_str(), "r");
		if (m_in == NULL) {
			printf("can not open in %s\n", m_inName.c_str());
			doexit();
		}

		m_outName = prepath + input + "_out.in";
	}


	IoBase::~IoBase()
	{
		fflush(NULL);
		fclose(m_in);
		fclose(m_out);
	}

}