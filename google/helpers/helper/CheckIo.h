#pragma once

#include "IoBase.h"

namespace helpers {

	class CheckIo : public IoBase
	{
	public:
		CheckIo(const std::string &input, const std::string &prepath = "");
		~CheckIo();
	};

}
