#pragma once

#include <string>

#include "IoBase.h"

namespace helpers {


	class Io : public IoBase

	{
	public:
		std::string m_debName;
		FILE *m_deb = NULL;

		Io(const std::string &input, const std::string &prepath = "");
		~Io();

		std::string readLine();

		int outf(_In_z_ _Printf_format_string_ const char *format, ...) __attribute__((format(printf, 2, 3)));;

		int debf(_In_z_ _Printf_format_string_ const char *format, ...) __attribute__((format(printf, 2, 3)));;
	};


}
