#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include <string>

using namespace std;

namespace fantastory {
namespace google {
namespace code_jam {

	class ASolver
	{
	protected:
		FILE *m_fin = NULL;
		FILE *m_fout = NULL;

	public:
		ASolver();
		virtual ~ASolver();

		//return 0 on success
		virtual int solve() = 0;

		int solveFile(const string &filepath);

	private:
		int do_solveFile(const string &filepath);
	};

	template <typename Solver>
	static int runFile(const string &filepath) {
		Solver solver;
		return solver.solveFile("C-test.in");
	}

}}}
