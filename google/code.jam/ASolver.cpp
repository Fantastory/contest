#include "stdafx.h"
#define _CRT_SECURE_NO_WARNINGS 1
#include "ASolver.h"

namespace fantastory {
namespace google {
namespace code_jam {

ASolver::ASolver()
{
}


ASolver::~ASolver()
{
	if (m_fin) fclose(m_fin);
	m_fin = NULL;
	if (m_fout) fclose(m_fout);
	m_fout = NULL;
}

int ASolver::solveFile(const string &filepath) {
	int ret = do_solveFile(filepath);
	printf("done press key\n");
	char c;
	scanf("%c", &c);
	return ret;
}

int ASolver::do_solveFile(const string &filepath) {
	m_fin = fopen(filepath.c_str(), "r");
	if (!m_fin) {
		printf("can not open %s\n", filepath.c_str());
		return 1;
	}

	string outfilepath = filepath + "_out.txt";
	m_fout = fopen(outfilepath.c_str(), "w");
	if (!m_fout) {
		printf("can not open output %s\n", filepath.c_str());
		return 1;
	}
	printf("output is put to %s\n", filepath.c_str());

	unsigned caseCount = 0;
	int ret = fscanf(m_fin, "%u\n", &caseCount);
	if (ret != 1) {
		printf("can not read caseCount\n");
		return 1;
	}
	printf("caseCount %u\n", caseCount);

	for (unsigned caseNum = 1; caseNum <= caseCount; ++caseNum) {
		printf("running case %u\n", caseNum);
		fprintf(m_fout, "Case #%u: ", caseNum);

		ret = solve();

		if (ret != 0) {
			printf("case failed\n");
			return 1;
		}

		fprintf(m_fout, "\n");
	}
	return 0;
}




}}}
