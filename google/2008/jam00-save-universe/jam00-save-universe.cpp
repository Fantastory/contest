// jam00-save-universe.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>

#include <string>
#include <unordered_set>
#include <direct.h>

using namespace std;

int solveCase(int caseNumber, FILE *fin, FILE *fout);

int run() {
	printf("started\n");

	char buf[512];
	_getcwd(buf, 512);
	printf("cwd %s\n", buf);

	string ioFileName = "A-large-practice";
	string ioFileIn = ioFileName + ".in";
	FILE *fin = fopen(ioFileIn.c_str(), "r");
	if (!fin) {
		printf("can not open %s\n", ioFileIn.c_str());
		return 1;
	}
	
	string outputFile = "test_out.txt";
	string ioFileOut = ioFileName + "out.txt";
	FILE *fout = fopen(ioFileOut.c_str(), "w");
	if (!fout) {
		printf("can not open %s\n", ioFileOut.c_str());
		return 1;
	}

	int caseCount = 0;
	int ret = fscanf(fin, "%d", &caseCount);
	if (ret != 1) {
		printf("can not read caseCount\n");
		return 1;
	}

	for (int i = 0; i < caseCount; ++i) {
		solveCase(i+1, fin, fout);
	}

    return 0;
}

int main() {
	int ret = run();
	printf("done\n");
	char c;
	scanf("%c", &c);
	return ret;
}

int solveCase(int caseNumber, FILE *fin, FILE *fout) {
	unsigned engineCount = 0;
	int ret = fscanf(fin, "%d\n", &engineCount);
	if (ret != 1) {
		printf("can not read engineCount\n");
		return 1;
	}
	printf("engineCount %d\n", engineCount);

	const int bufSize = 128;
	char buf[bufSize];

	//skip next engineCount names, we just need to know how many engines are there
	for (unsigned i = 0; i < engineCount; ++i) {
		fgets(buf, bufSize, fin);
	}

	int querryCount = 0;
	ret = fscanf(fin, "%d\n", &querryCount);
	if (ret != 1) {
		printf("can not read querryCount\n");
		return 1;
	}
	printf("querryCount %d\n", querryCount);

	unordered_set<string> enginesUsed;
	int engineSwitches = 0;
	
	for (int i = 0; i < querryCount; ++i) {
		fgets(buf, bufSize, fin);

		enginesUsed.insert(buf);

		if (enginesUsed.size() >= engineCount) {
			engineSwitches++;
			enginesUsed.clear();
			enginesUsed.insert(buf);
		}
	}

	fprintf(fout, "Case #%d: %d\n", caseNumber, engineSwitches);

	return 0;
}
