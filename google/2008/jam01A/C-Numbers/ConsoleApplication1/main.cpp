#include "stdafx.h"

#include <direct.h>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <stdint.h>
#include <assert.h>
#include <unordered_map>

using namespace std;

//string prepath = "..\\2016-C\\";
string prepath = "";
string testin = "test";
//string testin = "C-small-attempt0";
//string testin = "C-small-attempt1";
//string testin = "C-large";


int work();
int test();
int doexit() {
	printf("exiting\n");
	char c;
	scanf("%c", &c);
	exit(0);
}

int main()
{
	test();
	work();
	return 0;
}

int test() {

	printf("tests done\n");
	return 0;
}

int solve(FILE *fin, FILE *fout) {
	int64_t n;
	fscanf(fin, "%I64d\n", &n);
	printf("n: %I64d\n", n);
	
	int64_t mul = int64_t (sqrt(5) + 3);
	
	std::unordered_map<int64_t, unsigned> resultsSet;
	vector<int64_t> resultsVector;
	resultsVector.resize(1001);

	int64_t result = 1;

	for (int i = 0; i <= n; ++i) 
	{
		resultsVector[i] = result;
		if (resultsSet.count(result)) {
			printf("found cycle at n = %d it is %I64d\n", i, result);
			break;
		}
		resultsSet[result] = i;

		result *= mul;
		result %= 1000;
	}

	fprintf(fout, " %I64d\n", result);

	return 0;
}

int work() {
	string path = prepath + testin + ".in";
	FILE *fin = fopen(path.c_str(), "r");
	if (fin == NULL) {
		printf("can not open in %s\n", path.c_str());
		char *cwd = _getcwd(NULL, 0);
		printf("cwd %s\n", cwd);
		doexit();
	}

	path = prepath + testin + "_out.in";
	FILE *fout = fopen(path.c_str(), "w");
	if (fout == NULL) {
		printf("can not open out %s\n", path.c_str());
		doexit();
	}
	fflush(fout);

	int problemc;
	int ret = fscanf(fin, "%d\n", &problemc);
	if (ret != 1) {
		printf("problemc\n");
		doexit();
	}
	printf("problemc %d\n", problemc);

	for (int n = 0; n < problemc; ++n) {
		fprintf(fout, "Case #%d:", n + 1);
		printf("Case #%d:", n + 1);

		solve(fin, fout);
	}

	doexit();
	return 0;
}

