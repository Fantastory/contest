// B-Milkshakes.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <deque>
#include <vector>
#include <set>
#include <algorithm>
#include <functional>

#include "../../../code.jam/ASolver.h"

using namespace std;
using namespace fantastory::google;

class Customer {
public:
	std::set<unsigned> unmalted;

	//if it is -1 than he does not like any malted
	int malted = -1;
};

class Shake {
public:
	int malted = 0;
	std::set<Customer *> customersLikingUnmalted;
};

class Solver : public code_jam::ASolver {
public:
	int solve() {
		vector<Shake> milkshakes;
		vector<Customer> customers;

		unsigned shakeCount;
		fscanf(m_fin, "%u", &shakeCount);
		//mark all shakes as unmalted
		//they count them from 1 not from 0
		milkshakes.resize(shakeCount+1);
		
		unsigned customerCount;
		fscanf(m_fin, "%u", &customerCount);
		customers.resize(customerCount);
		
		set<Customer *> unsatisfied;

		for (Customer &c : customers) {
			fscanf(m_fin, "%u", &shakeCount);
			for (unsigned i = 0; i < shakeCount; ++i) {
				unsigned shakeNum; // number of a shake
				fscanf(m_fin, "%u", &shakeNum);

				unsigned malted; //if he likes it malted
				fscanf(m_fin, "%u", &malted);
				
				if (malted) {
					c.malted = shakeNum;
				}
				else {
					c.unmalted.insert(shakeNum);
					milkshakes[shakeNum].customersLikingUnmalted.insert(&c);
				}
			}

			if (c.unmalted.size() <= 0) {
				unsatisfied.insert(&c);
			}
		}


		//if we have some unsatisfied customers
		while (unsatisfied.size() > 0) {
			auto iter = unsatisfied.begin();
			Customer satisfying = **iter;
			unsatisfied.erase(iter);

			//lets malt something for him
			int shakeToMaltNum = satisfying.malted;
			if (shakeToMaltNum < 0) {
				fprintf(m_fout, "IMPOSSIBLE");
				return 0;
			}

			Shake &shakeToMalt = milkshakes[shakeToMaltNum];
			if (shakeToMalt.malted > 0) {
				//it is already malted, go to another guy
				continue;
			}
			shakeToMalt.malted = 1;

			//some guys like this shake unmalted, mark that they will not get it
			for (Customer *c : shakeToMalt.customersLikingUnmalted) {
				c->unmalted.erase(shakeToMaltNum);
				if (c->unmalted.size() == 0) {
					unsatisfied.insert(c);
				}
			}
		}

		//all customers satisfied
		for (unsigned i = 1; i < milkshakes.size(); ++i) {
			fprintf(m_fout, "%d ", milkshakes[i].malted);
		}

		return 0;
	}
	
};


int main()
{
	Solver solver;
	string file = "B-test.in";
	file = "B-small-practice.in";
	file = "B-large-practice.in";
	return solver.solveFile(file);
}

