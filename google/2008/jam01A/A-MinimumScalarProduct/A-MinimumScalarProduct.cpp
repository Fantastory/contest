// A-MinimumScalarProduct.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <string>
#include <deque>
#include <algorithm>
#include <functional>

#include "../../../code.jam/ASolver.h"

using namespace std;
using namespace fantastory::google;

class Vec {
public:
	deque<int64_t> values;

	int read(FILE *fin, unsigned size) {
		values.resize(size);

		for (unsigned i = 0; i < size; ++i) {
			fscanf(fin, "%I64u", &(values[i]));
		}

		std::sort(values.begin(), values.end());

		return 0;
	}

};

class Solver : public code_jam::ASolver {
public:

	int solve() {
		unsigned size = 0;
		fscanf(m_fin, "%u", &size);

		Vec v1;
		v1.read(m_fin, size);
		Vec v2;
		v2.read(m_fin, size);

		int64_t sum = 0;
		for (unsigned i = 0; i < size; ++i) {
			sum += v1.values.front() * v2.values.back();
			v1.values.pop_front();
			v2.values.pop_back();
		}

		fprintf(m_fout, "%I64d", sum);
		return 0;
	}

};

int main()
{
	Solver solver;
	string file = "A-test.in";
	file = "A-small-practice.in";
	file = "A-large-practice.in";
	return solver.solveFile(file);
}

