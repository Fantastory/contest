// jam00-save-universe.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>

#include <string>
#include <unordered_set>
#include <direct.h>
#include <map>

using namespace std;

int solveCase(int caseNumber, FILE *fin, FILE *fout);

int run() {
	printf("started\n");

	char buf[512];
	_getcwd(buf, 512);
	printf("cwd %s\n", buf);

	char problem = 'B';
	//string testSet = "-test";
	//string testSet = "-small-practice";
	string testSet = "-large-practice";
	string ioFileName = string(1, problem) + testSet;
	string ioFileIn = ioFileName + ".in";
	FILE *fin = fopen(ioFileIn.c_str(), "r");
	if (!fin) {
		printf("can not open in %s\n", ioFileIn.c_str());
		return 1;
	}

	string ioFileOut = ioFileName + "-out.txt";
	FILE *fout = fopen(ioFileOut.c_str(), "w");
	if (!fout) {
		printf("can not open out %s\n", ioFileOut.c_str());
		return 1;
	}

	int caseCount = 0;
	int ret = fscanf(fin, "%d", &caseCount);
	if (ret != 1) {
		printf("can not read caseCount\n");
		return 1;
	}

	for (int i = 0; i < caseCount; ++i) {
		solveCase(i + 1, fin, fout);
	}

	return 0;
}

int main() {
	int ret = run();
	char c;
	printf("done\n");
	scanf("%c", &c);
	return ret;
}

struct TimeInfo {
	unsigned outgoing = 0; //number of trains that goes out at this time
	unsigned readyToGo = 0; //number of trains that are ready to go at this time
};

int solveCase(int caseNumber, FILE *fin, FILE *fout) {
	unsigned turnAround = 0;
	int ret = fscanf(fin, "%d\n", &turnAround);
	if (ret != 1) {
		printf("can not read turnAround\n");
		return 1;
	}
	printf("turnAround %d\n", turnAround);

	unsigned tripCount[2];
	ret = fscanf(fin, "%d %d\n", &tripCount[0], &tripCount[1]);
	if (ret != 2) {
		printf("can not read aTrips, bTrips\n");
		return 1;
	}

	typedef map<int, TimeInfo> TimeMap;
	TimeMap timeMaps[2];

	char stations[2] = { 'A', 'B' };
	for (int station = 0; station < 2; ++station) {
		TimeMap *startMap = &timeMaps[0];
		TimeMap *endMap = &timeMaps[1];
		if (station > 0) {
			//i wonder it swaps only addresses
			swap(startMap, endMap);
		}

		for (unsigned i = 0; i < tripCount[station]; ++i) {
			unsigned startHour, startMinute;
			unsigned endHour, endMinute;

			ret = fscanf(fin, "%02d:%02d %02d:%02d\n",
				&startHour, &startMinute,
				&endHour, &endMinute);
			if (ret != 4) {
				printf("can not read times\n");
				return 1;
			}

			//
			//printf("%c %d:%d %d:%d\n", );

			startMinute += 60 * startHour;
			(*startMap)[startMinute].outgoing++;

			endMinute += 60 * endHour + turnAround;
			(*endMap)[endMinute].readyToGo++;
		}
	}
	
	//check how many trains each station needs
	unsigned trainsNeeded[2] = { 0,0 };
	for (int station = 0; station < 2; ++station) {
		TimeMap &timeMap = timeMaps[station];

		int trainsCurrentlyOnStation = 0;

		for (const auto & kv : timeMap)
		{
			const unsigned &k = kv.first;
			const TimeInfo &timeInfo = kv.second;

			unsigned hour = k / 60;
			unsigned minute = k % 60;
			printf("%c %02d:%02d ", stations[station], hour, minute);
			printf("%d + %d = %d | %d\t",    //wainting + ready = ready | needed
				trainsCurrentlyOnStation, timeInfo.readyToGo,
				trainsCurrentlyOnStation + timeInfo.readyToGo,
				timeInfo.outgoing
			);

			trainsCurrentlyOnStation += timeInfo.readyToGo;
			trainsCurrentlyOnStation -= timeInfo.outgoing;

			if (trainsCurrentlyOnStation < 0) {
				printf("needs %d new trains", -trainsCurrentlyOnStation);
				trainsNeeded[station] += -trainsCurrentlyOnStation;
				trainsCurrentlyOnStation = 0;
			}
			else {
				printf("no new trains");
			}
			printf("\n");

		}
	}

	fprintf(fout, "Case #%d: %d %d\n", caseNumber, trainsNeeded[0], trainsNeeded[1]);

	return 0;
}
