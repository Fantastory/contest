// jam00-c-Fly-Swatter.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <string>

#include "../../code.jam/ASolver.h"

using namespace std;
using namespace fantastory::google;

class FlySolver : public code_jam::ASolver {
private:
	double _R = 0; //large circle
	double _t = 0; //circle width
	double _c = 0; //small (inner) circle
	double _s = 0; //half of strand size
	double _g = 0; //gap beetwen strands

	unsigned _strandCount;

public:

	//TODO test
	void countStrands() {
		//lets check count horizot strands
		//better count open sqers in one line
		//so it a strand does not fully fill in circle there is no open square for it

		//0             < c <= s                   => 0 squares
		//s             < c <= s+g+s+s             => 1 squares
		//s+g+s+s       < c <= s+g+s+s+g+s+s       => 2 squares
		//s+g+s+s+g+s+s < c <= s+g+s+s+g+s+s+g+s+s => 3 squares
		//s+(g+2s)k     < c <= s+(g+2s)(k+1)       => k+1 squares

		//k < (c-s)/(g+2s) <= (k+1)       => k+1 squares
			
		if (_c <= _s) {
			_strandCount = 0;
		}
		else {
			double width = _c - _s;
			_strandCount = unsigned( (width + (_g + 2 * _s)) / (_g + 2 * _s) );
		}
		
		printf("_strandCount %u\n", _strandCount);
	}

	//where circle cuts line on heigth x, where it cuts (x=const,y=*), how it is noted in math?  0=x-X
	double getCY(double x) {
		//x^2 + y^2 = _c^2
		return sqrt(_c*_c - x*x);
	}

	double getCX(double y) {
		return getCY(y);
	}

	int solve() {
		double f; // fly size

		int ret = fscanf(m_fin, "%lf %lf %lf %lf %lf", &f, &_R, &_t, &_s, &_g);
		if (ret != 5) {
			printf("could not read something\n");
			return 1;
		}
		
		printf("%lf %lf %lf %lf %lf\n", f, _R, _t, _s, _g);
		//first decrease fly to a singel point;
		_t += f;
		_s += f;
		f -= f;
		//innerCircle = c = R-t;
		_c = _R - _t;
		printf("%lf %lf %lf %lf %lf\n", f, _R, _t, _s, _g);

		//we wiil check only 1 forth part of the circle
		//we are interested in iner circle: x^2 + y^2 = c^2
		countStrands();

		//count space of all squers lline by line, starting at x = s;
		double space = 0;
		double x = _s;
		while (x < _c) 
		{
			//x is bottom of squere
			//xt - it is top of squere
			double xt = x + _g;
			//check where circle cuts us
			double cy = getCY(xt);
			
			//now go right, check all squeres in the line
			double y = _s;
			while (y < cy)
			{
				//right most part of squere
				double yt = y + _g;
				double cx = getCX(yt);

				if      (cy >= yt && cx >= xt) {
					//whole squere in circle
					space += _g*_g;
				}
				else if (cy <  yt && cx >= xt)
				{
					//little rectangle
					double size = (xt - x) * (cy - y);
					//little triangle
					size += (xt - x) * (cy - y);
					//and part of circle
					size += 0;//arghhhh
				}
				else if (cy < yt && cx > xt)
				{

				}
				else if (cy < yt && cx > xt)
				{
				
				}
				else {
					printf("hey there is only 4 cases of a squere\n");
					int a = 0;
					int b = 6 / a;
					return 1;
				}
			}
		}

		return 0;
	}
};





int main()
{
	FlySolver solver;
	return solver.solveFile("C-test.in");
}
